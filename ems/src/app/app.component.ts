import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { EmployeeComponent } from './employee/employee.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  mess_child_employee:any;
  @ViewChild(EmployeeComponent,{static:true}) emppchild:any;
  message_from_department:any;
  
  ngAfterViewInit(): void {
    this.mess_child_employee=this.emppchild.mess_child;
  
  }
  title = 'ems';
  message_app="This message is from app component (Parent)"
}
