import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
  @Input('parentData') mess:any;
  mess_dep="This is from child(Department)";

  @Output() messageEvent= new EventEmitter();


  deptList=[
    {'deptno':101,'dname':'Accounting','lcode':301},
    {'deptno':102,'dname':'Production','lcode':302},
    {'deptno':103,'dname':'Developer','lcode':303},
    {'deptno':104,'dname':'Testing','lcode':304},
  ]
  constructor() { }
  onClickEvent(){
    this.messageEvent.emit(this.mess_dep);

  }

  ngOnInit(): void {
  }

}
