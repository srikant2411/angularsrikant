import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  mess_child="This message is from child(EmployeeChild)";

 @Input('parentData') mess:any;
  constructor() { }

  ngOnInit(): void {
  }

}
