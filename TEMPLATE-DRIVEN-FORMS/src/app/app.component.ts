import { Component } from '@angular/core';
import { Employee } from './employee';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TEMPLATE-DRIVEN-FORMS';
  employeeModel = new Employee("","","");
  submitUser(user:any){
    console.log(user);
  }
}
