import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  login(loginForm:NgForm){
    console.log(loginForm.value, loginForm.valid);

  }
  title = 'TemplateDrivenForms';
}
