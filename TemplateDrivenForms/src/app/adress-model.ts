export interface AdressModel {
    adress:string;
    city:string;
    state:string;
    postcode:number;
    country:any[];
    aggrement:boolean;
}
